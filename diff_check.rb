require 'diff/lcs'

escaped = File.read('escaped.json')
unescaped = File.read('unescaped.json')

diffs = Diff::LCS.diff(escaped, unescaped)
diffs.each do |diff|
  p diff
end
